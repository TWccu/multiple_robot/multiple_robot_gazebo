#!/usr/bin/env python
import rospy
from gazebo_msgs.srv import DeleteModel, SpawnModel
from geometry_msgs.msg import Pose
import random

import roslib; roslib.load_manifest('visualization_marker_tutorials')
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
import math

if __name__ == '__main__':    
    rospy.init_node("human_spawner")

    human_num = int(rospy.get_param("~spawner_human_num", 15))    

    # 載入刪除物件及新增物件的Service
    delete_model = rospy.ServiceProxy("gazebo/delete_model", DeleteModel)
    spawn_model = rospy.ServiceProxy("gazebo/spawn_urdf_model", SpawnModel)

    # rospkg.RosPack().get_path()可取得指定的ROS套件路徑
    with open(rospkg.RosPack().get_path('multiple_robot_gazebo')+"/object/human.urdf", "r") as f:
        product_xml = f.read()

    # 先刪除數量內的物件，避免上次新增的物件殘留，沒寫得很全面，加減用
    for num in xrange(0,human_num):
        item_name = "human_{0}".format(num)
        print("Deleting model:" + str(item_name))
        delete_model(item_name)

    # 新增物件
    for num in xrange(0,human_num):
        # 取得亂數，下面用來設置物件的x,y座標
        y_random = random.randint(-19,19)
        x_random = random.randint(-19,19)

        # 建立物件的資訊物件
        initial_pose = Pose()
        # 設置x,y座標，0.25只是減少重疊機率，不重要
        initial_pose.position.x = 0.25 * x_random 
        initial_pose.position.y = 0.25 * y_random

        # 設定物件名稱
        item_name = "human_{0}".format(num)
        print("Spawning model:" + str(item_name))
        
        #使用先前建立的新增物件Service，代入準備好的資料，產生物件。
        spawn_model(item_name, product_xml, "", initial_pose, "world")