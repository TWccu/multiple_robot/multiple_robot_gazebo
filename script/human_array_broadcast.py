#!/usr/bin/env python
import rospy
from multiple_robot_gazebo.msg import HumanGazebo, Human, HumanArray
from multiple_robot_gazebo.srv import UpdateHumanMarkerState, GetAllHumanData
from multiple_robot_nav.srv import AddNewRequirement, AddNewRequirementRequest
from visualization_msgs.msg import MarkerArray, Marker
from std_msgs.msg import ColorRGBA
import random
import copy

colorMap = {0: ColorRGBA(r=1, g=0, b=0, a=1),
            10: ColorRGBA(r=0, g=1, b=0, a=1),
            20: ColorRGBA(r=0, g=0, b=1, a=1),
            30: ColorRGBA(r=1, g=1, b=0, a=1),
            40: ColorRGBA(r=1, g=0, b=1, a=1),
            11: ColorRGBA(r=0, g=0.5, b=0, a=1),
            21: ColorRGBA(r=0, g=0, b=0.5, a=1),
            31: ColorRGBA(r=0.5, g=0.5, b=0, a=1),
            41: ColorRGBA(r=0.5, g=0, b=0.5, a=1)
            }    

class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)


def init_human_array(data, args):
    global human_list_index_to_human_dict_id
    human_array = args[0]
    marker_array = args[1]
    position_marker_array = args[2]
    human_dict = args[3]
    human_array.humans = data.humans
    marker_array.markers = []
    human_list_index_to_human_dict_id = []
    position_marker_array.markers = []
    for human_data in data.humans:
        if not human_dict.has_key(human_data.id):
            human_dict[human_data.id] = human_data
        human_list_index_to_human_dict_id.append(human_data.id)
        # add human CYLINDER marker
        marker = Marker()
        marker.header.frame_id = "/map"
        marker.type = marker.CYLINDER
        marker.action = marker.ADD
        marker.id = human_data.id
        marker.scale.x = 0.2
        marker.scale.y = 0.2
        marker.scale.z = 0.2
        # marker.color = colorMap[0]
        marker.color = colorMap[human_dict[human_data.id].state]
        marker.pose.orientation.w = 1.0
        marker.pose.position.x = human_data.pose.position.x
        marker.pose.position.y = human_data.pose.position.y 
        marker.pose.position.z = human_data.pose.position.z
        marker_array.markers.append(marker)

        # add position text marker
        position_marker =  copy.deepcopy(marker)
        position_marker.type = position_marker.TEXT_VIEW_FACING
        position_marker.pose.position.z = 0.23
        position_marker.scale.z = 0.1
        position_marker.scale.x = 0
        position_marker.scale.y = 0
        position_marker.color.r = 1
        position_marker.color.g = 1
        position_marker.color.b = 1
        position_marker.text = str(human_data.id) + ": (x:" + str(position_marker.pose.position.x) + ", y:" + str(position_marker.pose.position.y) + ")"
        position_marker_array.markers.append(position_marker)

    # global sub_once
    # sub_once.unregister()

human_dict = {}
human_list_index_to_human_dict_id = []

# Service
def updateHumanMarkerState(req):
    global human_dict
    try:
        human_dict[req.id].state = req.state
        return "OK"
    except:
        return "Can't find id"
    # global marker_array, human_array
    # for index, item in enumerate(human_array.humans):
    #     if item.id == req.id:
    #         item.state = req.state
    #         marker_array.markers[req.id].color = colorMap[req.state]
    #         return "OK"
    # return "Can't find id."

def getAllHumanData(req):
    global human_array
    return human_array

def dynamicRequirement():
    global human_array, addNewRequirement, human_dict
    # for human in human_array.humans:
    for human in human_dict.values():
        if random.randint(0,5) == 1:
            if human.state%10 is not 0 and random.randint(0,100) == 1:
                human.state = 0
                req = {'id':human.id, 'state':human.state}
                req = Struct(**req)
                updateHumanMarkerState(req)
                service_req = AddNewRequirementRequest()
                service_req.human_id = human.id
                try:
                    addNewRequirement(service_req)
                except:
                    pass
                break

if __name__ == '__main__':

    # Initialize ros node
    rospy.init_node("human_array_broadcast", log_level=rospy.DEBUG)

    is_dinamic_requirement = bool(rospy.get_param('~is_dinamic_requirement', False))
    rospy.logerr(is_dinamic_requirement)
    # initialize needed object and parameters
    human_array = HumanArray()
    marker_array = MarkerArray()
    position_marker_array = MarkerArray()

    maker_publisher = rospy.Publisher("visualization_marker_array", MarkerArray, queue_size=10)
    position_publisher = rospy.Publisher("position_marker_array", MarkerArray, queue_size=10)
    human_array_publisher = rospy.Publisher("human_state_array", HumanArray, queue_size=10)

    rate  = rospy.Rate(10)

    # Subscribe initialize humans data
    human_state_sub = rospy.Subscriber("human_array", HumanArray, init_human_array, (human_array, marker_array, position_marker_array, human_dict))

    # Create Services
    updateHumanMarkerService = rospy.Service('update_human_marker_state', UpdateHumanMarkerState, updateHumanMarkerState)
    getAllHumanDataService = rospy.Service('get_all_human_data', GetAllHumanData, getAllHumanData)

    # Connect Service
    addNewRequirement = rospy.ServiceProxy('add_new_requirement', AddNewRequirement)


    while not rospy.is_shutdown():
        if len(human_array.humans) > 0:
            maker_publisher.publish(marker_array)
            position_publisher.publish(position_marker_array)
            human_array_publisher.publish(human_array)
            if is_dinamic_requirement:
                dynamicRequirement()
            rate.sleep()