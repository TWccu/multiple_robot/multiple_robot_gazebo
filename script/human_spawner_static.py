#!/usr/bin/env python
import rospy
import roslib; roslib.load_manifest('visualization_marker_tutorials')
import sys
import rospkg 

from gazebo_msgs.srv import DeleteModel, SpawnModel
from geometry_msgs.msg import Pose
from multiple_robot_gazebo.msg import HumanGazebo, Human, HumanArray

def publish_human_array(markerArray):
    topic = 'visualization_marker_array'
    publisher = rospy.Publisher(topic, MarkerArray, queue_size=100)
    publisher.publish(markerArray)


human_position_array = [[3.54, 1.83],[-0.01, -3.56],[3.39, -0.86],[-0.61, 3.25],[0.56, -3.86],
                        [1.09, 3.68],[1.22, 0.64],[1.04, -3.54],[-1.07, 2.79],[1.28, 1.29],
                        # [0.96, -2.03],[-0.20, -1.94],[1.81, -0.32],[-0.82, -0.21],[-1.28, -0.38],
                        # [3.70, -3.56],[3.80, 3.89],[0.33, 2.35],[-1.87, 3.97],[-1.32, 1.00],
                        # [-2.74, 1.62],[-2.20, -1.59],[-3.22, -0.25],[-2.51, 3.73],[2.70, 0.57],
                        ]


if __name__ == '__main__':
    rospy.init_node("human_spawner")

    delete_model = rospy.ServiceProxy("gazebo/delete_model", DeleteModel)
    spawn_model = rospy.ServiceProxy("gazebo/spawn_urdf_model", SpawnModel)

    with open(rospkg.RosPack().get_path('multiple_robot_gazebo')+"/object/human.urdf", "r") as f:
        product_xml = f.read()

    for num in xrange(0,len(human_position_array)):
        item_name = "human_{0}".format(num)
        print("Deleting model:" + str(item_name))
        delete_model(item_name)

    # create gazebo object
    humanGazeboArray = []
    for num in xrange(0,len(human_position_array)):

        initial_pose = Pose()
        initial_pose.position.x = human_position_array[num][0] 
        initial_pose.position.y = human_position_array[num][1]

        item_name = "human_{0}".format(num)
        human_gazebo = HumanGazebo(item_name, initial_pose)
        humanGazeboArray.append(human_gazebo)
        print("Spawning model:" + str(item_name))
        spawn_model(item_name, product_xml, "", initial_pose, "world")

    # create rviz object
    humanArray = HumanArray()
    for num in xrange(0,len(human_position_array)):
        human_gazebo = humanGazeboArray[num]
        human = Human(id=num, type=0, state=0)
        human.pose = human_gazebo.pose
        humanArray.humans.append(human)

    publisher = rospy.Publisher("human_array", HumanArray, queue_size=10)

    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        publisher.publish(humanArray)

    # while not rospy.is_shutdown():
    #     connections = publisher.get_num_connections()
    #     if connections > 0:
    #         publisher.publish(humanArray)
    #         break  