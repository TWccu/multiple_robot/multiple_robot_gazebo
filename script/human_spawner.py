#!/usr/bin/env python
import rospy
import roslib; roslib.load_manifest('visualization_marker_tutorials')
import math
import random
import rospkg 

from gazebo_msgs.srv import DeleteModel, SpawnModel
from geometry_msgs.msg import Pose
from multiple_robot_gazebo.msg import HumanGazebo, Human, HumanArray

if __name__ == '__main__':
    rospy.init_node("human_spawner")

    human_num = int(rospy.get_param("~spawner_human_num", 15))

    delete_model = rospy.ServiceProxy("gazebo/delete_model", DeleteModel)
    spawn_model = rospy.ServiceProxy("gazebo/spawn_urdf_model", SpawnModel)

    with open(rospkg.RosPack().get_path('multiple_robot_gazebo')+"/object/human.urdf", "r") as f:
        product_xml = f.read()

    for num in xrange(0,human_num):
        item_name = "human_{0}".format(num)
        print("Deleting model:" + str(item_name))
        delete_model(item_name)

    # create gazebo object
    humanGazeboArray = []
    for num in xrange(0,human_num):
        y_random = random.randint(-10, 10)
        x_random = random.randint(-10, 10)

        initial_pose = Pose()
        initial_pose.position.x = 0.25 * x_random 
        initial_pose.position.y = 0.25 * y_random

        item_name = "human_{0}".format(num)
        human_gazebo = HumanGazebo(item_name, initial_pose)
        humanGazeboArray.append(human_gazebo)
        print("Spawning model:" + str(item_name))
        spawn_model(item_name, product_xml, "", initial_pose, "world")

    # create rviz object
    humanArray = HumanArray()
    for num in xrange(0,human_num):
        human_gazebo = humanGazeboArray[num]
        human = Human(id=num, type=0, state=0)
        human.pose = human_gazebo.pose
        humanArray.humans.append(human)

    publisher = rospy.Publisher("human_array", HumanArray, queue_size=1)

    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        publisher.publish(humanArray)

    # while not rospy.is_shutdown():
    #     connections = publisher.get_num_connections()
    #     if connections > 0:
    #         publisher.publish(humanArray)
    #         break  
