#!/usr/bin/env python
import rospy
from multiple_robot_gazebo.srv import UpdateHumanMarkerState, GetAllHumanData
from geometry_msgs.msg import PoseWithCovarianceStamped
from gazebo_msgs.srv import SetModelState
from gazebo_msgs.msg import ModelState
from nav_msgs.msg import OccupancyGrid
from std_srvs.srv import Empty

agv_init_pose = {"robot1":{'x':-4, 'y':-3}, "robot2":{'x':-3, 'y':-4}}

simple_human_list = {}

def getAllHumanList():
    simple_human_list = {}
    get_all_human_data = rospy.ServiceProxy('get_all_human_data', GetAllHumanData)
    humans = get_all_human_data().human_array.humans
    return len(humans)

def updateHumanMarkerState(id, state):
    update_human_marker_state = rospy.ServiceProxy('update_human_marker_state', UpdateHumanMarkerState)
    update_human_marker_state(id=id, state=state)



if __name__ == "__main__":

    rospy.init_node("initAllStateNode")
    rate = rospy.Rate(5) # 10hz
    set_AGV_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
    human_num = getAllHumanList()
    for i in range(0,human_num):
        updateHumanMarkerState(i, 0)
    for key, value in agv_init_pose.items():

        state_msg = ModelState()
        state_msg.model_name = key
        state_msg.pose.position.x = value['x']
        state_msg.pose.position.y = value['y']
        set_AGV_state(state_msg)

        clear_costmap = rospy.ServiceProxy(key + "/move_base/clear_costmaps", Empty)
        empty = Empty()
        
        pub = rospy.Publisher(key+'/initialpose', PoseWithCovarianceStamped, queue_size=1)
        pose = PoseWithCovarianceStamped()
        pose.header.stamp = rospy.Time.now()
        pose.header.frame_id = 'map'
        pose.pose.pose.position.x = value['x']
        pose.pose.pose.position.y = value['y']
        # pose.pose.pose.orientation.z = 0.785398
        pose.pose.pose.orientation.w = 0.785398
        pose.pose.covariance=[0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.06853891945200942]
        counter = 0
        while counter < 10:
            connections = pub.get_num_connections()
            if connections > 0:
                counter = counter + 1
                pub.publish(pose)
                clear_costmap()
                rate.sleep()

        