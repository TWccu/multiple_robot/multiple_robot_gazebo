# Summary

Date : 2020-04-29 16:12:10

Directory /home/jihungmycena/catkin_ws/src/multiple_robot/multiple_robot_gazebo/script

Total : 6 files,  270 codes, 49 comments, 77 blanks, all 396 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Python | 6 | 270 | 49 | 77 | 396 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 6 | 270 | 49 | 77 | 396 |

[details](details.md)