# Details

Date : 2020-04-29 16:12:10

Directory /home/jihungmycena/catkin_ws/src/multiple_robot/multiple_robot_gazebo/script

Total : 6 files,  270 codes, 49 comments, 77 blanks, all 396 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [script/__init__.py](/script/__init__.py) | Python | 0 | 0 | 1 | 1 |
| [script/human_array_broadcast.py](/script/human_array_broadcast.py) | Python | 108 | 19 | 21 | 148 |
| [script/human_spawner.py](/script/human_spawner.py) | Python | 41 | 8 | 15 | 64 |
| [script/human_spawner_non_marker.py](/script/human_spawner_non_marker.py) | Python | 28 | 10 | 10 | 48 |
| [script/human_spawner_static.py](/script/human_spawner_static.py) | Python | 45 | 10 | 17 | 72 |
| [script/initAllState.py](/script/initAllState.py) | Python | 48 | 2 | 13 | 63 |

[summary](results.md)