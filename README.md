# 專案預覽圖

- 於Gazebo中建置AGV與自訂物件（人形）
![alt text](img/gazebo.png "gazebo")

- 於Rviz中顯示AGV各資訊及產生自訂物件Marke
![alt text](img/rviz.png "rviz")

# Demo
1. Create world

        $ roslaunch multiple_robot_gazebo stage1_world.launch 
2. Add human

        $ roslaunch multiple_robot_gazebo human_service_static.launch  

# 專案內容簡述
本專案目的為在模擬環境Gazebo中，建立多台AGV並可增加自定義物件達到往後避障、巡檢之開發需求。

# 檔案結構
- __img：__
    僅放置本專案展示圖片用。
- __launch：__
    放置因需求不同而結合不同Node的launch file。
- __maps：__
    放置預先掃描(slam)完地圖檔案，啟動map_server時需要載入使用的地圖。
- __msg：__
    放置自定義Topic資料結構的定義檔。
- __object：__
    管理自訂義物件的模型資料，包含ROS用、Gazebo用及原始3D模型的stl檔
    - /meshes： 
        放置3D模型的stl檔
- __script：__
    放置Python程式碼的資料夾，其功能通常都為ROS的Node
- __src：__
    與script性質一樣，差別在於程式碼是使用C++撰寫，本專案使用Python所以沒有該資料夾
- __srv：__
    與msg類同，用途為定義Service的資料結構
- __urdf：__
    放置AGV相關的模型資料，一樣與Object有ROS用、Gazebo用及部份零件的stl檔
- __worlds：__
    放置Gazebo的環境模型資料

[註] 這邊沒有rviz相關的資料，rviz個人是定義在navigation上需要的內容，所以放置另一專案中。

# 模擬環境建構流程
1. 使用gazebo_ros產生空白世界

    (gazebo_ros: http://wiki.ros.org/gazebo_ros?distro=melodic)
2. 載入預用環境模型及部份參數設置
3. 定義好AGV相關的模擬資料
4. 使用gazebo_ros讀取AGV的模擬資料後載入世界
5. (optional) 依照需求放入物件，人形、機臺、障礙物等...

# 各流程及配置檔案內容細節
接下來以Demo中使用的"stage1_world.launch"及"human_service_static.launch"做說明，各.launch檔案可於launch資料夾中查看。
## 1. 使用gazebo_ros產生空白世界
於"stage1_world.launch"中的14行開始可看到此步驟的內容，首先使用gazebo_ros套件中的empty_world.launch。

(gazebo_ros: http://wiki.ros.org/gazebo_ros?distro=melodic)


launch file中，簡單可分為5種資料：
1. __include__：
    用來執行其他launch file        
2. __node__：
    用來執行ros最小的運行單位Node，通常含有以下參數。
    
    - name：node的使用名稱，會覆蓋node程式中已定義好的名稱。
    - pkg：所使用ROS的專案名稱。
    - type：所使用專案中的特定node名稱。
3. __arg__：
    argument用來定義變數資料，使用範圍僅限於launch file中，並可使用"$(arg xxx)"來引用。
4. __param__：
    parameter也是拿來定義變數資料用的，但使用範圍擴增到整著ROS system都可使用，定義後可使用rosparam來查詢、使用。
5. __group__：
    可用來群組以上資料，便於管理與分割，使用group群組資料後，node、topic、param等名稱都會加上group的名稱當作前綴。

    `非常重要，多數套件會有topic、service或param的依賴關係，使用group管理後記得將依賴所使用到的名稱都更改正確，於下面內容及Navigation文件中也會在稍做說明。`

## 2. 載入預用環境模型及部份參數設置
15~20行為empty_world.launch中的變數資料，主要將"world_name"改為自訂的環境資料即可。
本專案使用的"stage1.world"可於"worlds"中查看。

## 3. 定義好AGV相關的模擬資料
因為在Gazebo建立模擬用的AGV需要許多配置，這步驟所涵蓋的內容會稍為複雜、繁瑣。
相對應到"stage1_world.launch"中的內容為24~25行。

1. 首先24行中為載入AGV的3D模型資料，因Gazebo中需要使用xacro編碼來建立3D物件，所以需要先將事先準備的AGV配置檔案經過xacro.py進行轉檔。

    [註]xacro與urdf差別不大，但多一些精細的參數，詳細可自行查找

## 4. 使用gazebo_ros讀取AGV的模擬資料後載入世界
相對應到"stage1_world.launch"中的內容為29~37行，其中40~48跟是相同的內容，只是group相異。

1. 第二步於36行使用"gazebo_ros"的"spawn_model"來載入前面建立的"urdf_file"，並設定需要參數後加入Gazebo中。

    - -urdf：表示原模型檔案為urdf格式的。
    - -model：物件名稱
    - -x：x座標
    - -y：y座標
    - -Y：面向角度
    - -param：所使用的模型檔案。(這邊於robot_description多加"/",原因為robot_description是group外的param，類似上一層的意義)

2. 最後一步於group中建立"robot_state_publisher"(31~33行)，該功用為不斷發送AGV的座標及狀態，以便往後於rviz上做呈現，該套件會配合tf轉換各種座標參數。於33行中為了與其他AGV的資訊相沖，也將"tf_prefix"更改為AGV的名稱。

    於下面連結可得知，該node會讀取"robot_description"的param來做使用，這也是於25行建立"robot_description"參數的原因之一。

    (robot_state_publisher: http://wiki.ros.org/robot_state_publisher)

## 5. (optional) 依照需求放入物件，人形、機臺、障礙物等...
此部份以本專案加入人形物件為例，且分為兩種方法：    

1. 使用gazebo_ros的spawn_model節點(node)
2. 於程式碼中使用spawn_model的Service，該Service會步驟1產生Gazebo模擬環境時一起建立。
(gazebo_ros的詳細功能：http://gazebosim.org/tutorials/?tut=ros_comm)

    ## 5-1. 使用spawn_model node新增物件
    該部份與建立AGV模型並載入的作法相同，可以開啟"human_create.launch"做查看，差別在沒有使用"robot_state_publisher"來將物件的狀態廣播出去（現實環境除非特殊處理，物件也不會自動發送狀態的），剩下的步驟就只是載入模型資訊，然後加入Gazebo環境中即可。如"human_create.launch"中3行為載入模型資訊，5~22行為重複加入物件，注意一個spawn_model node只能建立一個物件，需要多個物件則需要建立多的node，且name必須相異。

    ## 5-2. 使用spawn_model的Service新增物件
    此部份可開啟script資料夾中的"human_spawner_non_marker.py"做參考：
        
    - 20行：初始化ros node，取名為human_spawner。

    - 23~24行：建立串接Service的物件，串接了刪除物件的"gazebo/delete_model"以及新增物件的"gazebo/spawn_urde_model"。串接Service時也需要給定溝通的資料格式，程式碼中的"DeleteModel"、"SpawnModel"。\
    [註]`rosservice list`可列出所有的Service

    - 27~28：與使用spawn_model node相同，須先載入模型資訊。

    - 31~34：使用刪除物件的Service刪除指定名稱的物件，該部份沒有寫得很全面加減用而已。

    - 37~53：設定座標及物件名稱後，使用新增物件的Service建立物件於Gazebo中。

# 模型資料
前面有提到使用的模型資料可以分為三種類型：
1. 3D模型的stl檔，通常放於meshes資料夾中區分。
2. ROS本身描述物件的urdf或xacro檔
3. Gazebo使用的物理特性描述檔，通常為xacro檔


## 3D模型Stl檔
製作3D模型時目前測試是使用SketchUp來製作模型，而製作時有一點必須要注意：`表面`必須朝外，若表面沒有處理好，在Gazebo中會有透明的意外發生。

## ROS描述檔
此部份可參考urdf/turtlebot3_waffle_pi中的turtlebot3_waffle_pi.urdf.xacro檔，以下介紹幾個較為重要的元件：

1. __xacro:include__：為配置檔加入其他配置檔，功能類似合併配置檔，較好管理特殊的配置。而在urdf檔中，會直接使用include，並沒有xacro的前綴。其中又以載入.gazebo.xacro的配置檔最為特殊。

2. [__link__](http://wiki.ros.org/urdf/XML/link)：元件的最小單位，可以是抽象的空間或是實際的方塊、圓柱等。
    
    - __visual__：元件視覺的資訊，包含外型、材質(顏色)、座標偏移量等。
        
        - origin：座標偏移量。
        - geometry：幾何的外型，可使用 _mesh_ 直接載入3D模型的stl檔。
        - material：材質資訊，範例中的"light_black"，定義在common_properties.xacro中，可在自行前往參考。

    - __collision__：碰撞範圍資訊。可參照範例直接使用指定大小的 _box_ 當作碰撞資訊，或是同visual使用mesh來配置更精確放撞範圍。

    - __inertial__：元件的物理性質，也是最為複雜的配置參數。
    
        - mass：質量。
        - inertia：慣性，含有ixx, ixy, ixz, iyz, izz參數。\
        尚未研究出明確的配置規則，可預先以mass:ixx:iyy:izz=1:4:4:4，其他為0來配置。

3. [__joint__](http://wiki.ros.org/urdf/XML/joint)：連接兩 _link_ 的結構化配置，可調整type來呈現不同的結構化特性。

    - __parent__：父元件。
    - __child__：連接的子元件。
    - __origin__：基於父元件的接點偏移量。

(官方參考資料：http://wiki.ros.org/urdf/XML)

## Gazebo描述檔
另一複雜的配置檔，可參考urdf/turtlebot3_waffle_pi中的turtlebot3_waffle_pi.gazebo.xacro。
該配置檔大多數是參考ROS的描述檔，並在各元件上加入模擬特性，如攝影機、光達、資料傳輸Topic等...

範例7~51行，僅是將前面建立好的元件，加上其他物理特性等，以下是各屬性用途：

### `<gazebo>` Elements For Links


<table><tr>
<th>Name</th>
<th>Type</th>
<th>Description</th>
</tr>
<tr>
<td>material</td>
<td>value</td>
<td>Material of visual element</td>
</tr>
<tr>
<td>gravity</td>
<td>bool</td>
<td>Use gravity</td>
</tr>
<tr>
<td>dampingFactor</td>
<td>double</td>
<td>Exponential velocity decay of the link velocity - takes the value and multiplies the previous link velocity by (1-dampingFactor).</td>
</tr>
<tr>
<td>maxVel</td>
<td>double</td>
<td>maximum contact correction velocity truncation term.</td>
</tr>
<tr>
<td>minDepth</td>
<td>double</td>
<td>minimum allowable depth before contact correction impulse is applied</td>
</tr>
<tr>
<td>mu1</td>
<td rowspan="2">double</td>
<td rowspan="2">Friction coefficients μ for the principal contact directions along the contact surface as defined by the
  <a href="http://www.ode.org">Open Dynamics Engine (ODE)</a>
  (see parameter descriptions in <a href="http://www.ode.org/ode-latest-userguide.html#sec_7_3_7">ODE's user guide</a>)
</td>
</tr>
<tr>
<td>mu2</td>
</tr>
<tr>
<td>fdir1</td>
<td>string</td>
<td>3-tuple specifying direction of mu1 in the collision local reference frame.</td>
</tr>
<tr>
<td>kp</td>
<td rowspan="2">double</td>
<td rowspan="2">Contact stiffness k_p and damping k_d for rigid body contacts as defined by ODE
  (<a href="http://www.ode.org/ode-latest-userguide.html#sec_7_3_7">ODE uses erp and cfm</a>
  but there is a
  <a href="https://bitbucket.org/osrf/gazebo/src/b4d836c3ab3b0a/gazebo/physics/ode/ODEJoint.cc#ODEJoint.cc-982">mapping between erp/cfm and stiffness/damping</a>)
</td>
</tr>
<tr>
<td>kd</td>
</tr>
<tr>
<td>selfCollide</td>
<td>bool</td>
<td>If true, the link can collide with other links in the model.</td>
</tr>
<tr>
<td>maxContacts</td>
<td>int</td>
<td>Maximum number of contacts allowed between two entities. This value overrides the max_contacts element defined in physics.</td>
</tr>
<tr>
<td>laserRetro</td>
<td>double</td>
<td>intensity value returned by laser sensor.</td>
</tr>
</table>

範例53開始加入其他特殊的感測器以及控制特性：
    
- 53行：加入imu感測器。

- 61行：差速驅動裝置以及其發布的Topic等資料。

- 84行：imu感測器的細節配置。

- 113行：加入光達元件以及細節配置。

- 147行：加入照相機以及細節配置。

其中\<plugin\>的部份可參照官方提供的模擬配置：http://gazebosim.org/tutorials?tut=ros_gzplugins

(參考資料：https://www.guyuehome.com/388)

(官方參考資料：http://gazebosim.org/tutorials?tut=ros_urdf&cat=connect_ros)